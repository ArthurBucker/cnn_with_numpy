# project CNN
import numpy as np

from layers import input_layer
from layers import conv_layer
from layers import pooling_layer
from layers import activation_layer
from layers import flatten_layer
from layers import hidden_layer
from layers import output_layer
from test_cnn import *

import matplotlib.image as mpimg
import matplotlib.pyplot as plt

# very nice refence that I used to build this code:     http://cs231n.github.io/convolutional-networks/


class CNN:

  def __init__(self, arc=[]):
    self.architecture_info = arc
    self.layers = []
    for i in range(len(self.architecture_info)):
      self.add_layer(self.architecture_info[i])
    return

  def add_layer(self, conf):
    l_type = conf["type"]

    if l_type == "input":
      self.layers.append(input_layer(conf))

    # !!!!!!! permit custom set of strides, initial_filter, initial_b....   !!!!!!!
    elif l_type == "conv":

      n_inputs = self.layers[-1].n_outputs
      n_outputs = conf["number_of_outputs"]
      filter_size = conf["filter_size"]
      act = conf["activation"] if "activation" in conf else None
      f = conf["initial_filter"] if "initial_filter" in conf else []
      print(act)

      if len(filter_size) == 2:
        filter_size.append(1)

      # self.layers.append(conv_layer(
      #     n_inputs , n_outputs , filter_size))

      self.layers.append(conv_layer(n_inputs, n_outputs, filter_size, input_size=[
      ], stride=[1, 1], initial_filter=f, initial_bias=[0], padding="None", activation=act))

    elif l_type == "max_pooling":
      scale = conf["scale"] if "scale" in conf else (2, 2)
      n_outputs = self.layers[-1].n_outputs

      self.layers.append(pooling_layer(
          scale_x=scale[0], scale_y=scale[1], n_outputs=n_outputs))

    elif l_type == "flatten_layer":
      # (n_samples, n_inputs, input_h, input_w,
      #  input_d) = self.layers[-1].get_out_data().shape

      self.layers.append(flatten_layer())
      # n_outputs=n_inputs * input_h * input_w * input_d))

    elif l_type == "activation":
      self.layers.append(activation_layer(
          mode=conf["mode"]))

    elif l_type == "hidden_layer":

      act = conf["activation"] if "activation" in conf else None
      weights = conf["weights"] if "weights" in conf else []
      n_inputs = conf["n_inputs"] if "n_inputs" in conf else self.layers[-1].n_outputs

      # print("test")
      self.layers.append(hidden_layer(
          n_inputs, conf["n_outputs"], activation=act, initial_weights=weights))
    elif l_type == "fully_connected":
      pass
    else:
      raise(self.achitecture[self.step], " is a invalid CNN step")

    return

  def predict(self, image_in):
    """runs through the list of layers"""
    # print(" --- layers dimentions  ---")
    for i, l in enumerate(self.layers):
      if i == 0:
        if type(self.layers[0]).__name__ != 'input_layer':
          raise("first layer needs to be an input_layer")
        else:
          self.layers[0].modify_input(image_in)
        # print(self.layers[i].get_out_data().shape)
        # print(self.layers[i].get_out_data())
      else:

        self.layers[i].run(self.layers[i - 1].get_out_data())
        # print(self.layers[i].get_out_data().shape)
        if i == 1 or i == 2:
          # print(self.layers[i].get_out_data()[0, 0, :, :, 0])
          pass
    return self.layers[-1].get_out_data()

  def train(self, train_data):
    bash_size = train_data["bash_size"] if "bash_size" in train_data else 1
    X = train_data["X"]
    Y = train_data["Y"]
    learning_rate = train_data["learning_rate"] if "learning_rate" in train_data else 0.001
    max_iterations = train_data["max_iterations"] if "max_iterations" in train_data else 2000
    if len(X) != len(Y):
      raise("X and Y have diffent sizes")

    for iteration in range(max_iterations):
      # split bashes
      total_error = 0

      for i in range(len(X) // bash_size):
        bash_x = X[i * bash_size:(i + 1) * bash_size]
        bash_y = Y[i * bash_size:(i + 1) * bash_size]
        p = self.predict(bash_x)

        # print(" - error: ")
        self.layers[-1].calculate_error(bash_y)  # global error
        # print("expected: " + str(bash_y) +
        #       " predicted" + str(p) +
        #       " erro: " + str(self.layers[-1].get_output_error()), end=" ")
        total_error += np.sum(
            np.abs(self.layers[-1].get_output_error()))
        # print("last: ")
        # print(self.layers[-1].get_output_error())

        for j in range(len(self.layers) - 1, 0, -1):  # propagates the error
          self.layers[j].propagate_error()
          # print("delta: " + str(d))
          next_layer_error = self.layers[j].get_input_error()
          # if(i == len(self.layers) - 1):
          #     print("2ND last")
          #     print(next_layer_error)
          self.layers[j - 1].set_output_error(next_layer_error)
        for j in range(1, len(self.layers)):
          self.layers[j].update_weights(learning_rate)

      if "test_iterations" in train_data:
        if iteration % train_data["test_iterations"] == 0:
          print(" ----- testset -------")
          teste = self.predict(X_im_test)
          print(teste)
          b_test = np.where(teste > 0.5, 1, 0)
          print(b_test)

      print("total erro:          ", end="")
      print(total_error)


def one_hot_key(targets):
  nb_classes = len(set(targets))
  return np.eye(nb_classes)[targets]


def main():
  arc = [{"type": "input", "size": [1, 8, 8, 1]},
         {"type": "conv", "number_of_outputs": 2,
          "filter_size": [3, 3], "activation":"leakyRelu"},
         {"type": "conv", "number_of_outputs": 2,
          "filter_size": [3, 3], "activation":"leakyRelu"},
         {"type": "max_pooling", "scale": [2, 2]},
         {"type": "flatten_layer"},
         {"type": "hidden_layer", "n_inputs": 8,
          "n_outputs": 4, "activation": "leakyRelu"},
         {"type": "hidden_layer", "n_outputs": 1, "activation": "sigmoid"}]

  pesos1 = np.array([[0.1, 0.2, 0.3, 0.4]])

  arc2 = [{"type": "input", "size": [1, 1, 4, 1]},
          {"type": "flatten_layer"},
          {"type": "hidden_layer", "n_inputs": 4, "activation": "leakyRelu",
              "n_outputs": 4},
          {"type": "hidden_layer", "activation": "sigmoid",
              "n_outputs": 2}]  # , "weights": np.transpose(pesos1)}]

  rede = CNN(arc)

  print(rede.layers)
  # print(rede.predict(image_unid))  # image defined at test_cnn

  train_data = {"X": X_im,
                "Y": Y_im,
                "learning_rate": 0.01,
                "bash_size": 8,
                "max_iterations": 20000,
                "test_iterations": 2000,
                "test_X": X_im_test,
                "test_Y": Y_im_test}
  rede.train(train_data)

  train = rede.predict(X_im)
  teste = rede.predict(X_im_test)
  print(train)
  print(teste)
  print(" ------- bin -------")
  b_train = np.where(train > 0.5, 1, 0)
  b_test = np.where(teste > 0.5, 1, 0)
  print(b_train)
  print(b_test)
  plt.figure(1)
  plt.imshow(np.reshape(rede.layers[1].filters[0, 0, :, 0], (3, 3)))
  plt.figure(2)
  plt.imshow(np.reshape(rede.layers[1].filters[1, 0, :, 0], (3, 3)))
  plt.show()


if __name__ == "__main__":
  main()
