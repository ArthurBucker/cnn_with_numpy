import mnist
import cnn
import numpy as np

print("inporting mnist dataset...")
x_train, t_train, x_test, t_test = mnist.load()
print("ok")

x_train = np.resize(x_train, [x_train.shape[0], 28, 28])
x_train = np.expand_dims(x_train, axis=1)
x_train = np.expand_dims(x_train, axis=-1)
t_train = cnn.one_hot_key(t_train)

x_test = np.resize(x_test, [x_test.shape[0], 28, 28])
x_test = np.expand_dims(x_test, axis=1)
x_test = np.expand_dims(x_test, axis=-1)
t_test = cnn.one_hot_key(t_test)

x_test = x_test.astype("float64")
x_train = x_train.astype("float64")
print("train: ")
print(x_train.shape)
print(t_train.shape)

print("test: ")
print(x_test.shape)
print(t_test.shape)

arc = [{"type": "input", "size": [1, 28, 28, 1]},
       {"type": "conv", "number_of_outputs": 32,
        "filter_size": [5, 5], "activation":"leakyRelu"},
       {"type": "max_pooling", "scale": [2, 2]},
       {"type": "conv", "number_of_outputs": 16,
        "filter_size": [3, 3], "activation":"leakyRelu"},
       {"type": "max_pooling", "scale": [2, 2]},
       {"type": "flatten_layer"},
       {"type": "hidden_layer", "n_inputs": 400,
        "n_outputs": 200, "activation": "leakyRelu"},
       {"type": "hidden_layer", "n_outputs": 10, "activation": "sigmoid"}]

rede = cnn.CNN(arc)

print(rede.layers)
# print(rede.predict(image_unid))  # image defined at test_cnn

train_data = {"X": x_train,
              "Y": t_train,
              "learning_rate": 0.01,
              "bash_size": 10,
              "max_iterations": 2000,
              "test_iterations": 20,
              "test_X": x_test,
              "test_Y": t_test}
rede.train(train_data)

train = rede.predict(X_im)
teste = rede.predict(X_im_test)
print(train)
print(teste)
print(" ------- bin -------")
b_train = np.where(train > 0.5, 1, 0)
b_test = np.where(teste > 0.5, 1, 0)
print(b_train)
print(b_test)
