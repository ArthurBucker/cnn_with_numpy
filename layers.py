import numpy as np
from test_cnn import *
import matplotlib.image as mpimg
import matplotlib.pyplot as plt


class layer(object):  # !!!!implement super layer class!!!
    """docstring for layer"""

    def __init__(self):
        super(layer, self).__init__()
        self.delta = 1

    def get_out_data(self):
        return self.out_data

    def get_in_data(self):
        return self.in_data

    def calculate_error(self, expected):
        self.output_error = expected - self.out_data

    def get_output_error(self):
        return self.output_error

    def set_output_error(self, error):
        self.output_error = error

    def get_input_error(self):
        return self.input_error

    def update_weights(self, learning_rate):
        pass


class input_layer(layer):
    """docstring for input_layer"""

    def __init__(self, param):
        super(layer, self).__init__()

        self.n_outputs = 1  # by default

        if len(param["size"]) == 2:  # problably only h and w with default n_input_images=1 d=1
            self.n_input = 1
            self.h = param["size"][0]
            self.w = param["size"][1]
            self.d = 1  # black and white

        elif len(param["size"]) == 3:
            self.h = param["size"][0]
            self.w = param["size"][1]
            self.d = param["size"][2]

        elif len(param["size"]) == 4:  # first dim is ignored, problably = number of samples
            self.h = param["size"][1]
            self.w = param["size"][2]
            self.d = param["size"][3]

        elif len(param["size"]) == 5:
            self.n_outputs = param["size"][1]
            self.h = param["size"][2]
            self.w = param["size"][3]
            self.d = param["size"][4]

    def modify_input(self, image_in):
        self.in_data = image_in.copy()
        if len(image_in.shape) == 2:  # problably only h and w with default deep=1
            image_in = np.expand_dims(image_in, axis=2)

        if len(image_in.shape) == 3:  # problably only h,w,d given -> add default sample size = 1
            image_in = np.expand_dims(image_in, axis=0)

        # problably only samples,h,w,d given -> add number of input images at time (input neurons)
        if len(image_in.shape) == 4:
            image_in = np.expand_dims(image_in, axis=1)

        self.out_data = image_in


class output_layer(layer):
    """docstring for output_layer"""

    def __init__(self, n_outputs):
        super(output_layer, self).__init__()

        self.n_outputs = n_outputs

    def feedforward(self, data_in):
        self.out_data = data_in

    # output.shape() = (sampes,neurons_values)    expected.shape() = (samples, expected_value)


class conv_layer(layer):
    """docstring for conv_layer"""

    def __init__(self, n_inputs, n_outputs, filter_size, input_size=[], stride=[1, 1], initial_filter=[], initial_bias=[], padding="None", activation=None):
        super(conv_layer, self).__init__()
        self.input_size = input_size
        self.n_outputs = n_outputs
        self.n_inputs = n_inputs
        self.stride = stride
        self.filter_size = filter_size
        self.padding_mode = padding

        if len(initial_filter) == 0:  # initialize random filters
            self.filters = np.random.rand(n_outputs, n_inputs,
                                          filter_size[0] * filter_size[1] * filter_size[2], 1)  # flatten_layer filters
        else:
            self.filters = initial_filter
            if len(self.filters.shape) == 3:  # missing filter deep, by defaut is setted to 1
                self.filters = np.expand_dims(self.filters, axis=0)

        if len(initial_bias) == 0:
            self.bias = np.random.rand(1, n_outputs, 1, 1)
        else:
            self.bias = initial_bias

        self.filters = self.filters.astype("float64")

        self.activation = activation_layer(mode=activation) if activation != None else None

    def run(self, image_in):
        self.out_data = self.convolve(image_in)

        if self.activation != None:
            self.activation.run(self.out_data)
            self.out_data = self.activation.get_out_data()

    def update_weights(self, learning_rate):
        # print("init_filter  \t", end=" ")
        # print(self.filters.shape)
        # # print("swaped filter\t", end=" ")
        # # print(np.swapaxes(self.filters, 0, 1).shape)
        # print("init_out_error\t", end=" ")
        # print(self.output_error.shape)
        # print("input shape   \t", end=" ")
        # print(self.in_data.shape)

        (filter_h, filter_w, filter_d) = self.filter_size
        (n_samples, n_outputs, output_error_h, output_error_w,
         output_error_d) = self.output_error.shape
        grad_filter = np.zeros_like(self.filters,dtype="float64")
        if self.activation != None:
            output_error = self.output_error* self.activation.delta

        for y in range(filter_h):
            for x in range(filter_w):
                grad_i = self.in_data[:,:,y:output_error_h+y:self.stride[1],x:output_error_w+x:self.stride[0],:]
                grad_i = np.expand_dims(grad_i,axis=1)* np.expand_dims(output_error,axis=2)
                grad_i = np.mean(grad_i,axis=(0,-2,-3))

                grad_filter[:,:,y*filter_h+x,:] = grad_i
        if self.activation != None:
            self.filters += grad_filter

        return
    def propagate_error(self):
        """this function takers the output error and propagates's it to each input
        the input error is guiven by the sum of each connected output error multiplied by the sinapse weight"""

        # print("init_filter  \t", end=" ")
        # print(self.filters.shape)
        # print("swaped filter\t", end=" ")
        # print(np.swapaxes(self.filters, 0, 1).shape)
        # print("init_out_error\t", end=" ")
        # print(self.output_error.shape)
        # print("input shape   \t", end=" ")
        # print(self.in_data.shape)

        if self.activation != None:
            self.activation.propagate_error()
        (filter_h, filter_w, filter_d) = self.filter_size
        self.input_error = np.zeros_like(self.in_data, dtype="float64")
        (n_samples, n_outputs, output_error_h, output_error_w,
         output_error_d) = self.output_error.shape

        # to propacagate i reversed de convolve process
        # reshape out put error
        reshaped_output_error = np.expand_dims(np.expand_dims(np.reshape(
            self.output_error, (n_samples, n_outputs, output_error_h * output_error_w, output_error_d)), axis=-3), axis=1)
        filters = np.expand_dims(np.swapaxes(self.filters, 0, 1), axis=-2)
        filters = np.repeat(filters, output_error_h *
                            output_error_w, axis=-2)

        # print("reshaed filter\t", end=" ")
        # print(filters.shape)

        # print("reshaped_out_error\t", end=" ")
        # print(reshaped_output_error.shape)

        raw_input_error = reshaped_output_error * filters
        # print("raw_input_error\t", end=" ")
        # print(raw_input_error.shape)

        raw_input_error = np.sum(raw_input_error, axis=2)
        raw_input_error = np.reshape(raw_input_error,(n_samples,self.n_inputs, filter_h, filter_w, output_error_h*output_error_w, filter_d))
        # print("sum_raw_input_error\t", end=" ")
        # print(raw_input_error.shape)

        for y in range(output_error_h):
            for x in range(output_error_w):
                self.input_error[:,:,y:y+filter_h,x:x+filter_w,:] += raw_input_error[:,:,:,:,y*output_error_w+x,:]
        # print("input_error_final  ",end="")
        # print(self.input_error.shape)
        # plt.figure(1)
        # plt.imshow(self.output_error[5,0,:,:,0])
        # plt.figure(2)
        # plt.imshow(self.in_data[5,0,:,:,0])
        # plt.figure(3)
        # plt.imshow(self.input_error[5,0,:,:,0])
        # plt.show()

        if self.activation != None:
            self.activation.propagate_error()  # calculate deltas

        return

    def convolve(self, image_in):  # input image comes as (samples, inputs_neurons, y, x, d)

        (filter_h, filter_w, filter_d) = self.filter_size

        # ----------- padding ------------
        if self.padding_mode != "None":
            image_in = self.padding(
                image_in, filter_h, filter_w, mode=self.padding_mode)
            # print(image_in.shape)
        self.in_data = image_in.copy()  # saves last image for backpropagation

        (n_samples, n_inputs, input_h, input_w, input_d) = image_in.shape

        output_h = input_h - (filter_h - 1)
        output_w = input_w - (filter_w - 1)
        output_d = input_d
        n_outputs = self.n_outputs
        resized_image = np.zeros(
            [n_samples, n_inputs, output_h * output_w, filter_w * filter_h * filter_d])

        # ----- resize to (samples, input_neurons, each possible space for the filter multiply in the image, n_filter_elements flat) -------
        # this method increases the speed of the algorithm,
        # check: http://cs231n.github.io/convolutional-networks/

        for y in range(output_h):
            for x in range(output_w):
                resized_image[:, :, output_w * y + x, :] = np.resize(
                    image_in[:, :, y:y + filter_h, x:x + filter_w, :], (n_samples, n_inputs, filter_h * filter_w * filter_d))

        # ------    dot product     -------
        # here we multiply the matrix of flatten_layer filters with the matrix of flat section of the input image

        # for more info check https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.tensordot.html
        prod = np.tensordot(resized_image, self.filters, axes=([3, 1], [2, 1]))
        # print(resized_image.shape) #(samples, input_n, input_w*h*d resized, windows areas flatten_layer = filter_w*filte_h*filter_d )
        # print(self.filters.shape)  #(filters_to_output_n, filters_from_input_n, filter_w*filte_h*filter_d, null)
        # print(prod.shape)          #(input_n, output_h*output_w*output_d, output_n, 1)
        # print(prod[0,:,0,0])
        # swap the axis 1 and 2 --> (input_n, output_n, output_h*output_w, deep)
        prod = np.swapaxes(prod, 1, 2)
        #---------- add bias ---------
        prod = prod + self.bias

        #--------  resize to normal image shape  -------

        out_image = np.resize(
            prod, (n_samples, n_outputs, output_h, output_w, output_d))
        # print(out_image.shape)
        # print(out_image[:,:,:,:,0])

        return out_image

    def padding(self, image_in, filter_h, filter_w, mode="zeros"):
        self.in_data = image_in.copy()

        (n_samples, n_inputs, input_h, input_w, input_d) = image_in.shape

        new_image = np.zeros(
            [n_samples, n_inputs, input_h + filter_h - 1, input_w + filter_w - 1, input_d])
        # print(new_image.shape)
        # margin on           top                   botton                   left                       right
        margin = [int(np.floor((filter_h - 1) / 2)), int(np.ceil((filter_h - 1) / 2)),
                  int(np.floor((filter_w - 1) / 2)), int(np.ceil((filter_w - 1) / 2))]

        if mode == "zeros":
            new_image[:, :, margin[0]:-margin[1],
                      margin[2]:-margin[3]] = image_in
        else:
            pass  # !!!!! add "same" and "full" mode  !!!!!!

        self.out_data = image_in.copy()
        return new_image


class pooling_layer(layer):
    """docstring for pooling_layer"""

    def __init__(self, scale_x=2, scale_y=2, mode="max_pooling", n_outputs=None):
        super(pooling_layer, self).__init__()
        self.scale_x = scale_x
        self.scale_y = scale_y
        if n_outputs != None:
            self.n_outputs = n_outputs

    def run(self, image_in):
        self.out_data = self.pool(image_in)
        pass

    def pool(self, image_in):  # !!!!!!! add max position recorder !!!!!!!
        """ performs max pooling_layer with the given scale on x and y,
        if the size of the image_in is not divisable by the scale, extra lines ar ignored"""
        # print("pooling")

        (n_samples, n_inputs, input_h, input_w, input_d) = image_in.shape
        new_image = np.zeros(
            [n_samples, n_inputs, input_h // self.scale_y, input_w // self.scale_x, input_d])
        pooling_max_mask_new = np.zeros_like(new_image)
        self.pooling_max_mask = np.zeros_like(image_in)
        for y in range(input_h // self.scale_y):
            for x in range(input_w // self.scale_x):
                sub_img = image_in[:, :, y * self.scale_y:(
                    y + 1) * self.scale_y, x * self.scale_x:(x + 1) * self.scale_x, :]
                # new_image[:, :, y, x, :] = np.amax(sub_img, axis=(2, 3))
                sub_img_flat = np.reshape(
                    sub_img, [n_samples, n_inputs, self.scale_y * self.scale_x, input_d])
                idx = sub_img_flat.argmax(axis=2)  # indexes of max values
                S, I, D = np.ogrid[:n_samples, :n_inputs, :input_d]
                new_image[:, :, y, x, :] = sub_img_flat[S, I, idx, D]
                blank = np.zeros_like(sub_img_flat)
                blank[S, I, idx, D] = 1
                self.pooling_max_mask[:, :, y * self.scale_y:(y + 1) * self.scale_y, x * self.scale_x:(x + 1) * self.scale_x, :] = np.reshape(
                    blank, [n_samples, n_inputs, self.scale_y, self.scale_x, input_d])
                pooling_max_mask_new[:, :, x, y, :] = idx

        # print("max_indexes")
        # print(self.pooling_max_mask[0, 0, :, :, 0])

        return new_image

    def propagate_error(self):
        # print("out_error")
        # print(self.output_error)
        self.input_error = np.zeros_like(self.pooling_max_mask)
        self.input_error[self.pooling_max_mask ==
                         1] = self.output_error.flatten()
        # print("in_error")
        # print(self.input_error[0, 0, :, :, 0])
        return


class activation_layer(layer):
    """docstring for activation_layer"""

    def __init__(self, mode="relu"):
        super(activation_layer, self).__init__()

        self.activation_mode = mode
        modes = ["relu", "sigmoid", "leakyRelu"]
        if not mode in modes:
            print("unknown activation mode: " +
                  mode + ", please try one of these: ")
            print(modes)
        self.output_error = 1
        self.leaky_alpha = 0.05

    def run(self, image_in):
        self.activate(image_in)

    def propagate_error(self):
        self.input_error = self.output_error
        self.calculate_delta()

    def calculate_delta(self):
        if self.activation_mode == "sigmoid":
            self.delta = self.deriv_sigmoid(self.out_data)
        elif self.activation_mode == "relu":
            self.delta = self.deriv_relu(self.in_data)
        elif self.activation_mode == "leakyRelu":
            self.delta = self.deriv_leakyRelu(self.in_data)
        else:
            self.delta = self.deriv_relu(self.in_data)
        return self.delta

    def activate(self, image_in):
        self.in_data = image_in.copy()
        if self.activation_mode == "sigmoid":
            self.out_data = self.sigmoid(image_in)
        elif self.activation_mode == "relu":
            self.out_data = self.relu(image_in)
        elif self.activation_mode == "leakyRelu":
            self.out_data = self.leakyRelu(image_in)
        else:
            self.out_data = self.relu(image_in)

    def relu(self, image_in):
        return np.maximum(image_in, 0)

    def sigmoid(self, image_in):
        return 1 / (1 + np.exp(-image_in))

    def leakyRelu(self, image_in):
        return np.maximum(image_in, image_in * self.leaky_alpha)

    def deriv_relu(self, z):
        return np.where(z > 0, 1, 0)

    def deriv_leakyRelu(self, z):
        return np.where(z > 0, 1, self.leaky_alpha)

    def deriv_sigmoid(self, z):
        return z * (1 - z)


class normalization(layer):
    """docstring for normalization"""

    def __init__(self, arg):
        super(normalization, self).__init__()
        self.arg = arg


class flatten_layer(layer):
    """docstring for flatten_layer"""

    def __init__(self, n_outputs = None):
        super(flatten_layer, self).__init__()
        if n_outputs != None:
            self.n_outputs = n_outputs
    def run(self, image_in):
        self.in_data = image_in.copy()
        self.out_data = self.flat(image_in)

    def flat(self, image_in):  # image_in shape = (samples, neurons, h, w, d)

        (n_samples, n_inputs, input_h, input_w, input_d) = image_in.shape

        flatten_data = np.reshape(
            image_in, (n_samples, n_inputs * input_h * input_w * input_d))
        return flatten_data

    def propagate_error(self):
        (n_samples, n_inputs, input_h, input_w, input_d) = self.in_data.shape
        self.input_error = np.reshape(
            self.output_error, (n_samples, n_inputs, input_h, input_w, input_d))


class hidden_layer(layer):
    """docstring for hidden_layer"""

    def __init__(self, n_inputs, n_outputs, dropout=1, initial_weights=[], activation=None):
        super(hidden_layer, self).__init__()
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs

        if len(initial_weights) == 0:  # initialize random filters
            self.weights = np.random.rand(
                n_inputs + 1, n_outputs)  # =1 for bias
        else:
            self.weights = initial_weights

        self.activation = activation_layer(
            mode=activation) if activation != None else None

    def run(self, data):
        self.out_data = self.feedfoward(data)
        # print("out_data")
        # print(self.out_data)
        if self.activation != None:
            self.activation.run(self.out_data)
            self.out_data = self.activation.get_out_data()
            # print("out_data a")
            # print(self.out_data)

    def feedfoward(self, in_data):  # in_data.shape = (samples, n_inputs)

        samples, _ = in_data.shape

        # add bias
        in_data_bias = np.ones([samples, self.n_inputs + 1])
        in_data_bias[:, :-1] = in_data
        self.in_data = in_data_bias
        # calculate result
        out_data = np.dot(in_data_bias, self.weights)
        return out_data

    def propagate_error(self):
        """this function takers the output error and propagates's it to each input
        the input error is guiven by the sum of each connected output error multiplied by the sinapse weight"""

        self.input_error = np.dot(
            self.output_error, np.transpose(self.weights))[:, :-1]
        if self.activation != None:
            self.activation.propagate_error()
        # propagate

        return self.activation.delta

    def update_weights(self, learning_rate):

        # correct weights
        # print("w init ", end="")
        # print(self.weights.shape)
        # print("in d original = ", end="")
        # print(self.in_data.shape, end="  ")
        # print("in d = ", end="")
        # print()
        # print("delta original = ", end="")
        # print(self.activation.delta.shape, end="  ")
        # print("delta", end="")
        # print()
        # print("out_error = ", end="")
        # print(self.output_error.shape)

        if self.activation != None:
            in_d = np.transpose(
                np.repeat(np.mean(self.in_data, axis=0, keepdims=1), self.n_outputs, axis=0))
            delta = np.repeat(np.mean(self.activation.delta,
                                      axis=0, keepdims=1), self.n_inputs + 1, axis=0)
            bash_size = self.output_error.shape[0]

            # grad = learning_rate * in_d * delta * self.output_error
            grad = learning_rate * \
                np.dot(np.transpose(self.in_data),
                       self.activation.delta * self.output_error) / bash_size
            # print("in_data")
            # print(self.in_data)
            # print("out_erro" + str(self.activation.out_data))
            # print(self.activation.output_error)
            # print("weights")
            # print(self.weights)
            # print("delta")
            # print(self.activation.delta)
            # print("grad: ")
            # print(-np.mean(grad, axis=1, keepdims=1))

            self.weights = self.weights + grad
        else:
            self.weights = self.weights - learning_rate * \
                self.output_error  # ----------- modificar -------------

    def calculate_error(self, expected):
        if self.activation == None:
            self.output_error = np.mean(
                expected - self.out_data, axis=0, keepdims=1)
        else:
            # print("exp")
            # print(expected)
            # print("out d")
            # print(self.out_data)
            self.activation.calculate_error(expected)
            self.output_error = self.activation.get_output_error()
            # print("out e")
            # print(self.output_error)


# for test
if __name__ == "__main__":
    # cnn.main()
    pass


def old_test():

    data_train_X = np.array([
        [0, 0],
        [1, 0],
        [0, 1],
    ])

    data_train_Y = np.array([
        [0, 1, 1],
        [1, 0, 0]
    ])

    # weights.shape() = inputs+1, outputs
    pesos1 = np.array([[0.1, 0.2], [0.3, 0.4], [0.5, 0.6]])
    h1 = hidden_layer(2, 2, dropout=1, initial_weights=pesos1)

    out = output_layer(2)

    in_data = np.array([[1, 0]])
    out_h1 = h1.feedfoward(data_train_X)
    print(h1.get_out_data())

    out.calculate_error(data_train_Y)
    print(h1.backpropagation(out.get_output_error()))
    """
    # imagex, init_filter, init_bias in test_cnn file
    conv = conv_layer((8,8,1), 2, 2, (3,3,1), initial_filter = init_filter,
                      initial_bias = init_bias, padding="zeros")
    imagex = conv.convolve(imagex)

    a = activation_layer()
    imagex = a.activate(imagex)
    print (imagex[:,:,:,:,0])

    p = pooling_layer()
    imagex = p.pool(imagex)

    """
