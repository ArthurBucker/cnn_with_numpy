# CNN with numpy
This repo contains a simple CNN implemented in python using only numpy as a external library

Reference link (contains all the basic theory behind cnn):
http://cs231n.github.io/convolutional-networks/

## checklist                 --     status

###feedforward:

implement layers classes     --     in progress
	conv                     --     ok
	pooling                  --     ok
	flat                     --     ok
	fully connected          --     ok


###backpropagation:

implement layers classes     --     ok
	conv                     --     ok
	pooling                  --     ok
	flat                     --     ok
	fully connected          --     ok

custom training              --     to be done
	dropout
	Otimization algorithms
