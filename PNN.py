import numpy as np

#fully conected NN

class PNN(object): 
	"""peceptron Neural net work"""
	def __init__(self, arc, initial_weights = []):
		super(PNN, self).__init__()
		self.architecture_info = arc
		self.layers = []
		for i in range(len(arc)-1):
			if len(initial_weights) == 0:
				self.add_layer(arc[i],arc[i+1])
			else:
				self.add_layer(arc[i],arc[i+1],initial_weights = initial_weights[i])
		return
		
	def add_layer(self, n_inputs, n_outputs, initial_weights = []):
		self.layers.append(hidden_layer(n_inputs,n_outputs, initial_weights = initial_weights))

	def run(self, in_data):
		for i,h in enumerate(self.layers):
			if i == 0:
				h.feedfoward(in_data)
			else:
				self.layers[i].feedfoward(self.layers[i-1].get_out_data())
		return self.layers[-1].get_out_data()

class hidden_layer(object):
	"""docstring for hidden_layer"""
	def __init__(self, n_inputs, n_outputs, dropout = 1, initial_weights = []):
		super(hidden_layer, self).__init__()
		self.n_inputs = n_inputs

		if len(initial_weights) == 0: #initialize random filters
			self.weights = np.random.rand(n_inputs+1, n_outputs)# =1 for bias
		else:
			self.weights = initial_weights

	def feedfoward(self, in_data): #in_data.shape = (samples, n_inputs)
		self.in_data = in_data

		samples,_ = in_data.shape

		#add bias
		in_data_bias = np.ones([samples, self.n_inputs+1])
		in_data_bias[:,:-1] = in_data

		#calculate result
		out_data = np.dot(in_data_bias, self.weights)


		self.out_data = out_data
		return
	
	def get_out_data(self):
		return self.out_data

	def get_in_data(self):
		return self.in_data
		
		
#test
if __name__ == "__main__":
	init_weights = np.array([[[0.5],[1],[1]]])

	r = PNN([2,1], init_weights)
	print(r.run(np.array([[2,2]])))
